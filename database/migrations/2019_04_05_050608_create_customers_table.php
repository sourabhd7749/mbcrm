<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_code');
            $table->string('name');
            $table->string('email');
            $table->enum('active', ['1', '0']);
            $table->string('abn');
            $table->enum('credit_status', ['Contract' , 'On Account' , 'Stopped']);
            $table->string('address');
            $table->string('suburb');
            $table->string('post_code');
            $table->string('city');
            $table->string('state');
            $table->text('note');
            $table->enum('billing_details', ['1', '0']);
            $table->string('billing_suburb');
            $table->string('billing_post_code');
            $table->string('billing_city');
            $table->string('billing_state');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
