<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $customers = [
            [
                'company_code' => 'Abc123',
                'name' => 'Company1',
                'email' => 'company1@email.com',
                'active' => '1',
                'abn' => 'CMP123',
                'credit_status' => 'Contract',
                'address' => '123 Lane, Vice City',
                'suburb' => 'Vice',
                'post_code' => '411011',
                'city' => 'Vice',
                'state' => 'GTA',
                'note' => 'All The Data entered is fake',
                'billing_details' => '1',
                'billing_suburb' => 'Vice',
                'billing_post_code' => '411011',
                'billing_city' => 'City',
                'billing_state' => 'GTA',
            ],

        ];

        DB::table('customers')->insert($customers);
    }
}
