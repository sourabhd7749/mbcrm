<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!--begin::Page Vendors Styles(used by this page) -->
        <link href="{{ asset('/vendors/custom/fullcalendar/fullcalendar.bundle.css')}} " rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="{{ asset('/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->
        <link href="{{ asset('/demo/default/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/demo/default/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/demo/default/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/demo/default/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />
        <!--end::Layout Skins -->
        <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
</head>
    <body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
            @include('layouts._partials._header')
            <!-- begin:: Content -->
            <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
            @yield('content')
            </div>
        </div>

    @include('layouts._partials._footer')
    <!-- Scripts -->
            <!--begin::Global Theme Bundle(used by all pages) -->
        <script src="{{asset('/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

        <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('/app/custom/general/dashboard.js')}}" type="text/javascript"></script>

        <!--end::Page Scripts -->

        <!--begin::Global App Bundle(used by all pages) -->
        <script src="{{asset('/app/bundle/app.bundle.js')}}" type="text/javascript"></script>

        <!--end::Global App Bundle -->
    <script>
        $(function(){
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": false,
              "positionClass": "toast-top-right",
              "preventDuplicates": false,
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut",
              "opacity": 1
            };

            @if(session()->has('message.level'))
                toastr.{{ session('message.level') }}("{!! session('message.content') !!}");
            @endif
        });
    </script>
    @stack('pageScripts')
</body>
</html>
