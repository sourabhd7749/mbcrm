@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--last m-portlet--head-lg m-portlet--responsive-mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-wrapper">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="fas fa-book"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Add a new programme
                        </h3>
                    </div>
                </div>

                {{--
                <div class="m-portlet__head-tools">
                    <a href="{{ url('admin-hub/courses/create') }}" class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
                        <span>
                            <i class="la la-close"></i>
                            <span>Cancel</span>
                        </span>
                    </a>

                    <a href="#" class="btn btn-brand m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
                        <span>
                            <i class="la la-check"></i>
                            <span>Save</span>
                        </span>
                    </a>

                </div>
                --}}

                </div>
            </div>
            <!--begin::Form-->
            <form method="POST" action="{{ url('admin-hub/courses') }}" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
                @csrf
                @include('courses._course_form')
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
@endsection

@push('pageScripts')
    <script src="{{ asset('vendors/summernote/summernote.min.js') }}" ></script>
    <script src="{{ asset('vendors/summernote/databasic/summernote-ext-databasic.js') }}" ></script>
    <script src="{{ asset('vendors/summernote/specialchars/summernote-ext-specialchars.js') }}" ></script>
    <script charset="utf-8">
        $(function () {
            var addLessonHTML = `<div class="row a-lesson" style="margin-left: 0; margin-right: 0">
                <div class="form-group m-form__group col-md-3" style="margin-top: 15px;">
                    <label for="">* Lesson — English</label>
                    <input name="name[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — English">
                </div>
                <div class="form-group m-form__group col-md-3">
                    <label for="name_dari">* Lesson — Dari</label>
                    <input name="name_da[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Dari">
                </div>
                <div class="form-group m-form__group col-md-3">
                    <label for="coursename_en_input">* Lesson — Pashto</label>
                    <input name="name_pa[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Pashto">
                </div>
                <div class="form-group m-form__group col-md-2">
                    <label for="coursename_en_input">* Days</label>
                    <input name="days[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Days">
                </div>
                <div class="form-group m-form__group col-md-1"><button type="button" class="btn m-btn--pill btn-danger btn-sm remove-lesson" style="margin-left: -30px; margin-top:30px;">Remove</button></div>
            </div>`;

            $('#addLesson').on('click', function(event) {
                $('#lessons').append(addLessonHTML);
                $('.remove-lesson').on('click', function(event) {
                    
                    $(this).closest('.a-lesson').remove();
                });
            });

            $('.remove-lesson').on('click', function(event) {
                $(this).closest('.a-lesson').remove();
            })

            $('.rich-text-editor').summernote({
                height: 300,
                placeholder: "Write here..."
            });
            $("input[type=file]").change(function () {
                var fieldVal = $(this).val();

                // Change the node's value by removing the fake path (Chrome)
                fieldVal = fieldVal.replace("C:\\fakepath\\", "");

                if (fieldVal != undefined || fieldVal != "") {
                    $(this).next(".custom-file-label").attr('data-content', fieldVal);
                    $(this).next(".custom-file-label").text(fieldVal);
                }
            });

            $("#m_selectpicker").selectpicker();
        });
    </script>
@endpush

@push('pageStyles')
    <link rel="stylesheet" href="{{ asset('vendors/summernote/summernote.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="{{ asset('vendors/summernote/databasic/summernote-ext-databasic.css') }}" type="text/css" media="screen" title="no title" charset="utf-8">
@endpush
