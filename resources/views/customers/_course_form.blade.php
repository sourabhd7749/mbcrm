<div class="m-portlet__body">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="m-form__section m-form__section--first">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">Course — English</h3>
        </div>
        <div class="form-group m-form__group">
            <label for="coursename_en_input">* Course Name — English</label>
            <input name="coursename_en" value="{{ old('coursename_en',
            $course->coursename_en) }}"type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Course name — English">
            <span class="m-form__help"></span>
        </div>
        <div class="form-group m-form__group">
            <label for="content_en">* Content — English</label>
            <textarea name="content_en" class="form-control m-input
                rich-text-editor" id="content_en" rows="5">{{ old('content_en',
                $course->content_en) }}</textarea>
        </div>
        <div class="m-form__group form-group">
            <label>* Status — English</label>
            <div class="m-radio-inline">
                <label class="m-radio m-radio--state-success">
                    <input type="radio" name="status_en" value="active"
                    @if(old('status_en', $course->status_en) == 'active') checked @endif> Active
                    <span></span>
                </label>
                <label class="m-radio m-radio--state-danger">
                    <input type="radio" name="status_en" value="inactive"
                    @if(old('status_en', $course->status_en) == 'inactive') checked @endif> Inactive
                    <span></span>
                </label>
            </div>
        </div>
    </div>
    <div class="row" style="margin-left:0; margin-right:0;">
        <div class="form-group m-form__group col-md-4" style="margin-top: 15px;">
            <label for="coursename_en_input">* Category</label>
            <select name="cat_id" class="form-control m-input" id="m_selectpicker">
                @foreach ($categories as $category)
                    <option name="" value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group m-form__group col-md-4">
            <label for="fees">* Fees</label>
            <input type="text" class="form-control m-input"
                            name="fees"
                            value="{{ old('fees', $course->fees) }}"
                            id="fees"
                            aria-describedby=""
                            placeholder="Fees">
        </div>
        <div class="form-group m-form__group col-md-4">
            <label for="exampleInputEmail1">* Featured Image</label>
            <div></div>
            <div class="custom-file">
                <input name='image' type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
    </div>

    <div class="m-form__section">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">Lessons</h3><button id="addLesson" type="button" class="btn m-btn--pill btn-primary btn-sm">Add new</button>
        </div>
    @forelse ($course->lessons as $lesson)
        <div id="lessons">
            <div class="row a-lesson" style=" margin-left: 0; margin-right: 0;">
                <div class="form-group m-form__group col-md-3" style="margin-top: 15px;">
                    <label for="">* Lesson — English</label>
                    <input name="name[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — English" value="{{ $lesson->name }}">
                </div>
                <div class="form-group m-form__group col-md-3">
                    <label for="name_dari">* Lesson — Dari</label>
                    <input name="name_da[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Dari" value="{{ $lesson->name_da }}">
                </div>
                <div class="form-group m-form__group col-md-3">
                    <label for="coursename_en_input">* Lesson — Pashto</label>
                    <input name="name_pa[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Pashto" value="{{ $lesson->name_pa }}">
                </div>
                <div class="form-group m-form__group col-md-2">
                    <label for="coursename_en_input">* Days</label>
                    <input name="days[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Days" value="{{ $lesson->days }}">
                </div>
                <div class="form-group m-form__group col-md-1"><button type="button" class="btn m-btn--pill btn-danger btn-sm remove-lesson" style="margin-left: -30px; margin-top:30px;">Remove</button></div>
            </div>
        </div>
    @empty
        <div id="lessons">
            <div class="row a-lesson" style=" margin-left: 0; margin-right: 0;">
                <div class="form-group m-form__group col-md-3" style="margin-top: 15px;">
                    <label for="">* Lesson — English</label>
                    <input name="name[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — English">
                </div>
                <div class="form-group m-form__group col-md-3">
                    <label for="name_dari">* Lesson — Dari</label>
                    <input name="name_da[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Dari">
                </div>
                <div class="form-group m-form__group col-md-3">
                    <label for="coursename_en_input">* Lesson — Pashto</label>
                    <input name="name_pa[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Pashto">
                </div>
                <div class="form-group m-form__group col-md-2">
                    <label for="coursename_en_input">* Days</label>
                    <input name="days[]" type="text" class="form-control m-input" id="coursename_en_input" aria-describedby="" placeholder="Lesson — Days">
                </div>
                <div class="form-group m-form__group col-md-1"><button type="button" class="btn m-btn--pill btn-danger btn-sm remove-lesson" style="margin-left: -30px; margin-top:30px;">Remove</button></div>
            </div>
        </div>
    @endforelse
    </div>

    <div class="m-separator m-separator--dashed m-separator--lg"></div>
    <div class="m-form__section">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">Course — Dari</h3>
        </div>
        <div class="form-group m-form__group">
            <label for="coursename_da_input">* Course Name — Dari</label>
            <input name="coursename_da" value="{{ old('coursename_da',
            $course->coursename_da) }}" type="text" class="form-control m-input" id="coursename_da_input" aria-describedby="" placeholder="Course name">
            <span class="m-form__help"></span>
        </div>
        <div class="form-group m-form__group">
            <label for="content_da">* Content — Dari</label>
            <textarea name="content_da" class="form-control m-input
                rich-text-editor" id="content_en" rows="5">{{ old('content_da', $course->content_da) }}</textarea>
        </div>
        <div class="m-form__group form-group">
            <label>* Status — Dari</label>
            <div class="m-radio-inline">
                <label class="m-radio m-radio--state-success">
                    <input type="radio" name="status_da" value="active"
                    @if(old('status_da', $course->status_da) == 'active') checked @endif> Active
                    <span></span>
                </label>
                <label class="m-radio m-radio--state-danger">
                    <input type="radio" name="status_da" value="inactive"
                    @if(old('status_da', $course->status_da) == 'inactive') checked @endif> Inactive
                    <span></span>
                </label>
            </div>
        </div>
    </div>
    <div class="m-separator m-separator--dashed m-separator--lg"></div>
    <div class="m-form__section">
        <div class="m-form__heading">
            <h3 class="m-form__heading-title">Course — Pashto</h3>
        </div>
        <div class="form-group m-form__group">
            <label for="coursename_pa_input">* Course Name — Pashto</label>
            <input name="coursename_pa" value="{{ old('coursename_pa', $course->coursename_pa) }}" type="text" class="form-control m-input" id="coursename_pa_input" aria-describedby="" placeholder="Course name">
            <span class="m-form__help"></span>
        </div>
        <div class="form-group m-form__group">
            <label for="content_pa">* Content — Pashto</label>
            <textarea name="content_pa" value="{{ old('content_pa', $course->content_pa) }}"
                class="form-control m-input rich-text-editor" id="content_pa"
                rows="5">{{ old('content_pa', $course->content_pa) }}</textarea>
        </div>
        <div class="m-form__group form-group">
            <label>* Status — Pashto</label>
            <div class="m-radio-inline">
                <label class="m-radio m-radio--state-success">
                    <input type="radio" name="status_pa" value="active"
                    @if(old('status_pa', $course->status_pa) == 'active') checked @endif> Active
                    <span></span>
                </label>
                <label class="m-radio m-radio--state-danger">
                    <input type="radio" name="status_pa" value="inactive"
                    @if(old('status_pa', $course->status_pa) == 'inactive') checked @endif> Inactive
                    <span></span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-brand m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
            <span>
                <i class="la la-check"></i>
                <span>Save</span>
            </span>
        </button>
        <button class="btn btn-danger m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
            <span>
                <i class="la la-close"></i>
                <span>Cancel</span>
            </span>
        </button>

        <button id="m_scroll_top" class="btn btn-outline-metal m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
            <span>
                <i class="la la-arrow-up"></i>
                <span>Go up</span>
            </span>
        </button>
    </div>
</div>
