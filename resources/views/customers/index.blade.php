@extends('layouts.app')

@section('content')
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Extended Pagination
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="{{ url('customers/create') }}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add a new course</span>
                        </span>
                    </a>
                </li>
                <li class="m-portlet__nav-item"></li>
                <li class="m-portlet__nav-item">
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                            <i class="la la-ellipsis-h m--font-brand"></i>
                        </a>
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">

        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="customers-table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Customer Code</th>
                    <th>Name</th>
                    <th>Title(pashto)</th>
                    <th>ABN</th>
                    <th>Credit Status</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('pageScripts')
    <script src="{{ asset('vendors/custom/datatables/datatables.bundle.js') }}"></script>
    {{-- <script src="{{ asset('app/custom/components/base/sweetalert2.js') }}"></script> --}}
    <script charset="utf-8">
        $(function() {
            $('#customers-table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: '{!! url('admin-hub/customers') !!}',
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'company_code', name: 'company_code' },
                        { data: 'name', name: 'name' },
                        { data: 'email', name: 'email' },
                        { data: 'abn', name: 'abn' },
                        { data: 'credit_status', name: 'credit_status' },
                        { data: 'address', name: 'address' },
                        { data: 'action', name: 'action', orderable: false, searchable: false }
                    ],
            });
        });
    </script>
@endpush

@push('pageStyles')
    <link href="{{ asset('vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet">
@endpush
